#The following is needed to allow PS1 to update after
#changing the current directory
setopt promptsubst

#Unset EQUALS to prevent '==' from being misinterpreted
#in zsh shell scripts (like this one).
#
#see man zshoptions|less -p EQUALS
#www.zsh.org/mla/users/2011/msg00161.html
unsetopt equals

HISTSIZE=1000
HISTFILE=$HOME/.zsh_history

bindkey -v #Vim keybindings

#Set autocompletion
if [ -e ~/.git-completion.sh ] ; then
  source ~/.git-completion.sh
fi
zstyle ':completion:*' use-cache yes
zstyle ':completion:*' menu select

# aliases that don't depend on which shell the user is using
if [ -e ~/.alias ] ; then
  source $HOME/.alias
fi

if [ -e ~/.private_aliases ] ; then
  source $HOME/.private_aliases
fi

# Zsh Shell functions
source $HOME/.zshfunctions
# Zsh Shell functions specific to work
# (.zshworkfunctions isn't included in git since it's work related)
if [ -e ~/.zshworkfunctions ] ; then
  source $HOME/.zshworkfunctions
fi

# Original green PS1
#PS1="%B%F{green}%n@%m%k %B%F{blue}%~${branch}
#
local branch='%B%F{cyan}$(current_branch)'
# Red/White PS1
PS1="%B%F{white}%n@%B%F{red}%m%k %B%F{blue}%~${branch}
%0(#.#.$) %b%f%k"
export PS1

export PATH="$HOME/.bin:$HOME/bin:$PATH"
#rbenv setup
if [ -e $HOME/.rbenv ] ; then
  export PATH="$HOME/.rbenv/bin:$HOME/bin:$PATH"
  eval "$(rbenv init - zsh)" # as per `rbenv init`
fi

#zsh-nvm
source ~/.zsh-nvm/zsh-nvm.plugin.zsh

# Set the default version of nvm to use
# Reference: https://eric.blog/2016/08/23/set-default-node-version-with-nvm/
# nvm alias default 0.12.7

#Enable Vim to show trailing whitespace in red. See ~/.vimrc for more
export TERM=xterm-256color

# David: why did I add the below line?? Maybe something else did?
#        Not sure so commenting out for now.
#        May have been for some window manager.
#export TERMINAL=urxvt

#export `dircolors -b $HOME/.dircolors`
