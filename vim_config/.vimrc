filetype plugin indent on
set ts=2 sts=2 sw=2 expandtab

" starting color configuration
syntax on
set termguicolors
set t_Co=256
set background=dark

let g:one_allow_italics=1 " allow italics when using the 'one' color scheme

let g:solarized_termcolors=256
let g:solarized_termtrans=1

"colo grb256
colo Base2Tone_SeaDark
"colo Base2Tone_MorningDark

" steampunk vibes
"colo Base2Tone_MotelDark

" good; 1st I used after discovering Base2Tone
"colo Base2Tone_MorningLight
"colo solarized
"colo d_grb256
"colo desert
"colo d_detailed

" only works on some terminals
"colo zellner
"colo solarized8
"colo monotonic


" ayu
"let ayucolor="dark"
"colo d_ayu


"colo tetragrammaton
"colo d_archery
"colo d_desert256
"colo citylights

" end color configuration

" line numbers
set number
set relativenumber

"set cursorline " highlight the current line

"https://www.reddit.com/r/vim/comments/rlxzz4/neovim_insert_mode/
"
" 1 or 0 -> blinking block
" 2 -> solid block
" 3 -> blinking underscore
" 4 -> solid underscore
" Recent versions of xterm (282 or above) also support
" 5 -> blinking vertical bar
" 6 -> solid vertical bar
" Insert Mode
let &t_SI .= "\<Esc>[6 q"
" Normal Mode
let &t_EI .= "\<Esc>[2 q"

let mapleader = "," " :h mapleader
set hlsearch "highlight search results

"Turn off indentLines plugin by default
let g:indentLine_enabled = 0

" If you notice help docs not being accessible for plugins
" Run `:helptags ALL` in Vim.
"
" https://vi.stackexchange.com/questions/17210/
" generating-help-tags-for-packages-that-are-loaded-by-vim-8s-package-management

" Disable the vim startify plugin from showing when you open vim
"
" NOTE: Sometimes this plugin is useful; however not all the time.
"       Hence why I like to disable it by default
"       but keep it around if I need it.
" Values: 1 means true, 0 means false.
let g:startify_disable_at_vimenter = 1

"Create a vertical bar at the 81st column
"autocmd FileType ruby,eruby,yaml setlocal colorcolumn=81
autocmd FileType * setlocal colorcolumn=81

"Set the syntax as 'conf' for any files with the '.local' extension
"http://vim.wikia.com/wiki/Forcing_Syntax_Coloring_for_files_with_odd_extensions
autocmd BufNewFile,BufRead *.local set syntax=conf

"Show extra whitespace config
"source: http://vim.wikia.com/wiki/Highlight_unwanted_spaces
"export TERM=xterm-256color
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()
"End of show extra whitespace config

" Start NERD Commenter configuration
"
" Align line-wise comment delimiters flush left
" instead of following code indentation
let g:NERDDefaultAlign = 'left'

" Allow commenting and inverting empty lines
" (useful when commenting a region)
let g:NERDCommentEmptyLines = 1
"
" End of NERD Commenter configuration

map <C-h> :nohl<cr>
map <Leader>i :IndentLinesToggle<CR>
map <Leader>h :nohl<cr>
map <Leader>f :put ='# frozen_string_literal: true'<CR>
map <Leader>r :put ='binding.pry'<CR>
map <Leader>rv :put ='<% binding.pry %>'<CR>
map <Leader>d :put ='debugger;'<CR>
map <Leader>ng :set notermguicolors <CR>
map <Leader>nn :set nornu <bar> :set nonu<CR>
map <Leader>nu :set rnu <bar> :set nu<CR>
map <Leader>nc :set colorcolumn= <CR>
" it.skip("Denver", async () => {
"  const name = "dino dude"
"  expect(name).toEqual("dino dude")
" })
map <Leader>t :put ='it(\"\", async () => { })'<CR>


" Paste whatever was last yanked
" Reference: https://www.brianstorti.com/vim-registers/
map <Leader>p "0p

"##################### Scrolling up/down ##############"
" Yes I (David) made these since I couldn't find
" any better solution to this problem where
" using C-u/C-d ALWAYS moved the screen instead
" of just moving the cursor a set number of lines up/down.
" Hitting 4k/4j repeatedly is tedious and moving the screen
" half of the page height is way too jarring in my opinion.
"
" The below is no longer needed since I'm remapping C-u/C-d.
" Leaving it here just incase I need it at some future time.
"set scroll=4 " Only scroll via Ctrl+U/Ctrl+D 4 lines at a time
nmap <C-u> 4k
nmap <C-d> 4j
"##################### END Scrolling up/down ##############"

"##### Start NeoVim tweaks ####
set nowildmenu
"set laststatus=1
"##### End NeoVim tweaks ####

" Handy Keybindings
"
" via: https://stackoverflow.com/questions/12128678/vim-go-to-beginning-end-of-next-method
" go to the beginning of a method: [m
" go to the end of a method:       ]M
"
